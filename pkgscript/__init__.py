#!/usr/bin/env python

from .pkgscript import import_parent_packages
from .version import __version__, __date__, __credits__
